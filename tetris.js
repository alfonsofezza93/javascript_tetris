/* Grid information */
gridSizeX = 300;                  /** Grid size into the canvas on X axis */
gridSizeY = 600;                  /** Grid size into the canvas on Y axis */
blockNumX = 10;                   /** Number of columns */
blockNumY = 20;                   /** Number of rows */
blockDim = gridSizeX/blockNumX;   /** Size of the block in pixels */
grid = [(blockNumX+2)*(blockNumY+2)];     /** Grid is an array of blocks */

/* Canvas information*/
borderX = gridSizeX/blockNumX;                     /** Distance from X border in pixels */            
borderY = gridSizeY/blockNumY;                     /** Distance from Y border in pixels */
canvasSizeX = 500;                /** Canvas size on X axis */
canvasSizeY = 650;                /** Canvas size on Y axis */

/** the first 2 elements is colomuns number and rows number respectly,
 *  the others is the emptyness row by row */
piecesArray = [                        /** Array of possible pieces */
  [2,2,1,1,1,1],                  /** Square */
  [3,2,0,1,1,1,1,0],              /** Left Z */
  [3,2,1,1,0,0,1,1],              /** Right Z */
  [3,2,1,0,0,1,1,1],              /** Left L */
  [3,2,0,0,1,1,1,1],              /** Right L */
  [4,1,1,1,1,1],                  /** Line */
  [3,2,0,1,0,1,1,1]               /** T */
];                               

/* Pieces information */
piece = {
  blockMap : [],                  /** Block map for drawing */
  hitBlockMap : [],               /** Index of the hitting block */
  dimX : 0,                       /** Number of columns */
  dimY : 0,                       /** Number of rows */
  gridPosX : 0,                   /** X Position of the top left corner in the grid */
  gridPosy : 0,                   /** Y Position of the top left corner in the grid */
  direction : 0,                  /** Direction: 0 no direction, 1 right, -1 left, 2 down */
  tralsation : 0,                 /** Traslation: 0 not traslate, 1 traslate */
  speed: 15                       /** Speed of the piece expressed in Frame count */
};

/* Grid functions */
/** Create the grid and assign all the blocks objects is composed of */
function createGrid() {
  for(i=-1;i<blockNumY+1;i++){                        /* Iterate for all rows using 2 more rows for the frame */
    for(j=-1;j<blockNumX+1;j++){                      /* Iterate for all columns using 2 more cols for the frame*/
      if(i<0||j<0||i>blockNumY-1||j>blockNumX-1){     /* Create a frame around playable grid */
        grid[getGridIndex(j,i)] = {                   /* Initialize frame block elements */
          isEmpty: 1,                                 /* Set as full */
          blkColor: color(220)                        /* Canvas background color */
        }
      }else{
        grid[getGridIndex(j,i)] = {                   /* Initialize grid block elements */
          isEmpty: 0,                                 /* Default is empty */
          blkColor: color(230)                        /* Default background color */
        }
      }
    }
  }
  // grid[getGridIndex(blockNumX,0)].isEmpty=1;
  // grid[getGridIndex(blockNumX,0)].blkColor=0;
  // print(getGridIndex(blockNumX,0))
}

/** Get correct array index from x and y position */
function getGridIndex(posX, posY){
  return ((posY+1)*(blockNumX+2))+(posX+1);         /* Discard the frame */
}

/** Print all the playable blocks of the grid */
function printGrid() {
  for(i=0;i<blockNumY;i++){                         /* Only print the playable grid */
    for(j=0;j<blockNumX;j++){
      fill(grid[getGridIndex(j,i)].blkColor);
      rect((j*blockDim)+borderX, (i*blockDim)+borderY, blockDim, blockDim);
    }
  }
}

/** Add the piece to the grid */
function addToGrid(){
  for(i=0;i<piece.dimY;i++){
    for(j=0;j<piece.dimX;j++){
      if(piece.blockMap[i*piece.dimX+j].isEmpty===1){                       /* If the piece is not empty */
        grid[getGridIndex(piece.gridPosX+j,piece.gridPosY+i)].isEmpty = 1;  /* Set block grid as full */
        grid[getGridIndex(piece.gridPosX+j,piece.gridPosY+i)].blkColor = piece.blockMap[i*piece.dimX+j].blkColor;
      }
    }   
  }
}

/** Remove lines from the grid */
function removeFromGrid(){

}

/* Piece functions */
/** Randomize a new piece and fill object fields */
function spawnNewPiece() {
  /* Make all random stuffs */
  idx = Math.floor(Math.random()*7);                 /* Randomize a shape from pieces Array */
  rColor = Math.floor(Math.random()*256);            /* Randomize red color */
  gColor = Math.floor(Math.random()*(256-100)+50);       /* Randomize green color, exlude lower and higher colors */
  bColor = Math.floor(Math.random()*(256-150)+75);       /* Randomize blue color, exlude much lower and higher colors */

  piece.dimX = piecesArray[idx][0];             /* Take number of columns */
  piece.dimY = piecesArray[idx][1];             /* Take number of rows */

  piece.gridPosX = Math.floor(blockNumX/2) - Math.floor(piece.dimX/2);  /* Assign starting X position in the grid */
  piece.gridPosY = 0;                                                   /* Assign starting Y position in the grid */

  for(i=0;i<(piece.dimX*piece.dimY);i++){       /* Iterating the right number of times */
    if(piecesArray[idx][i+2]===1){              /* Discarding the cols and rows size */
      piece.blockMap[i] = {                     /* Initialize the block map for print */
        isEmpty : 1,                            /* If is not empty */
        blkColor : color(rColor,gColor,bColor)  /* Use the randomized color values */
      }
    }
    else{
      piece.blockMap[i] = {                     
        isEmpty : 0,                            /* If is empty */
        blkColor : color(230)                   /* Use the background color */
      }
    }
    
  // console.log("iteration %d isEmpty: %d color: ",i,piece.blockMap[i].isEmpty,piece.blockMap[i].blkColor.toString());
  }
  // generatePieceHitBox();
  // console.log("idx: ",idx);
  // console.log("dimX: ",piece.dimX);
  // console.log("dimY: ",piece.dimY);
}

/** Generate the hitbox coordinates array */
function generatePieceHitBox(){
  k = 0;                                                    /* Reset the hitbox index */

  if(piece.direction===0||piece.direction===2) {             /* If direction is vertical store vertically */
    for(j=0;j<piece.dimX;j++){                                /* Loop on columns first */
      for(i=piece.dimY-1;i>=0;i--){                           /* Start from bottom */
        if(piece.blockMap[i*piece.dimX+j].isEmpty===1){       /* If this specific block is full */
          piece.hitBlockMap[k++] = {                          /* Add new element in the hitbox array */
            gridPosX : piece.gridPosX+j,                      /* Store the grid position on X axis */
            gridPosY : piece.gridPosY+i                       /* Store the grid position on Y axis */
          }
        }
      }
    }
    return;
  }
  if(piece.direction===-1) {                                  /* If direction is left store from left to right */
    for(i=piece.dimY-1;i>=0;i--){                             /* Start from bottom */
      for(j=0;j<piece.dimX;j++){                              /* Start from left */
        if(piece.blockMap[i*piece.dimX+j].isEmpty===1){       /* If this specific block is full */
          piece.hitBlockMap[k++] = {                          /* Add new element in the hitbox array */
            gridPosX : piece.gridPosX+j,                      /* Store the grid position on X axis */
            gridPosY : piece.gridPosY+i                       /* Store the grid position on Y axis */
          }
        }
      }
    }
    return;
  }
  if(piece.direction===1) {                                   /* If direction is right store from right to left */
    for(i=piece.dimY-1;i>=0;i--){                             /* Start from bottom */
      for(j=piece.dimX-1;j>=0;j--){                           /* Start from right */
        if(piece.blockMap[i*piece.dimX+j].isEmpty===1){       /* If this specific block is full */
          piece.hitBlockMap[k++] = {                          /* Add new element in the hitbox array */
            gridPosX : piece.gridPosX+j,                      /* Store the grid position on X axis */
            gridPosY : piece.gridPosY+i                       /* Store the grid position on Y axis */
          }
        }
      }
    }
    return;
  }
  // for(i=0;i<k;i++){
  //   print("hitbox vector coordinate: x(" + piece.hitBlockMap[i].gridPosX + ") y(" + piece.hitBlockMap[i].gridPosY + ")");
  // }
}

/** Print the piece into the grid */
function printPiece() {
  for(i=0;i<piece.dimY;i++){
    for(j=0;j<piece.dimX;j++){
      if(piece.blockMap[i*piece.dimX+j].isEmpty===1){       /* Print only if is not empty */
      posX = ((piece.gridPosX+j)*blockDim)+borderX;         /* Calculate X position in the canvas on X axis */
      posY = ((piece.gridPosY+i)*blockDim)+borderY;         /* Calculate X position in the canvas on Y axis */
      strokeWeight(2);                                      /* Set the width of the stroke */
      stroke(0);                                            /* Set the color of the stroke */
      fill(piece.blockMap[i*piece.dimX+j].blkColor);        /* Use the block color for filling */
      square(posX, posY, blockDim);                         /* Draw square in correct position */
      // noStroke();                                           /* Disable the stroke again */
    }
      // print("iteration "+ i+ " "+j);
      // print("idx "+ (i*piece.dimX+j) +"isEmpty "+ piece.blockMap[i*piece.dimX+j].isEmpty);
      // console.log("gridPosX: ",posX);
      // console.log("gridPosY: ",posY);    
      // print("color: "+piece.blockMap[i*piece.dimY+j].blkColor.toString());    
    }
  }
}

/** Game functions */
/** Check if the block can go down of one row */
function verticalHitBoxCheck() {
  i = 0;                                                      /* Reset hitbox array counter */
  posX = piece.hitBlockMap[i].gridPosX;                       /* Taxe the X and Y position of */
  posY = piece.hitBlockMap[i].gridPosY;                       /* the first hitbox array element (left bottom corner) */
  
  for(j=0;j<piece.dimX;j++){                                  /* Loop on columns, hitbox array is stored by columns */
    if(grid[getGridIndex(posX,(posY+1))].isEmpty===1) {           /* If the block below this hitbox's block is full */
      return 1;                                               /* Hit the grid and return */            
    }    

    if(++i>piece.hitBlockMap.length-1){                       /* Increase the counter and check for index overflow */
      break;                                                  /* If all the array is checked end the loop */
    }

    while((i<piece.hitBlockMap.length-1) &&                                 /* Loop until the hitbox array index is valid */
    (piece.hitBlockMap[i].gridPosX === piece.hitBlockMap[i-1].gridPosX)){   /* and until the X position is the same */
      i++;                                                                  /* skip this element because it's X position is already checked*/
    }

    posX = piece.hitBlockMap[i].gridPosX;                     /* Taxe the X and Y position of */
    posY = piece.hitBlockMap[i].gridPosY;                     /* the new hitbox array element */
  }

  return 0;                                                   /* The grid was not hitted */            
}

/** Check if the block can go left/right of one row */
function horizontalHitBoxCheck() {
  i = 0;                                                      /* Reset hitbox array counter */
  posX = piece.hitBlockMap[i].gridPosX;                       /* Taxe the X and Y position of */
  posY = piece.hitBlockMap[i].gridPosY;                       /* the first hitbox array element (left bottom corner) */

  for(j=0;j<piece.dimY;j++){                                  /* Loop on columns hitbox array is stored by columns */
    if(grid[getGridIndex((posX+piece.direction),posY)].isEmpty===1) {           /* If the block below this hitbox's block is full */
    return 1;                                               /* Hit the grid and return */            
    }    

    if(++i>piece.hitBlockMap.length-1){                       /* Increase the counter and check for index overflow */
      break;                                                  /* If all the array is checked end the loop */
    }

    while((i<piece.hitBlockMap.length-1) &&                                 /* Loop until the hitbox array index is valid */
    (piece.hitBlockMap[i].gridPosY === piece.hitBlockMap[i-1].gridPosY)){   /* and until the X position is the same */
      i++;                                                                  /* skip this element because it's X position is already checked*/
    }

    posX = piece.hitBlockMap[i].gridPosX;                     /* Taxe the X and Y position of */
    posY = piece.hitBlockMap[i].gridPosY;                     /* the new hitbox array element */
  }

  return 0;                                                   /* The grid was not hitted */            
}

/** Rotate the piece */
function rotatePiece() {
  rotBlockMap = [];

  k = 0;                                                    /* Reset the rotation index */
  for(j=0;j<piece.dimX;j++){                                /* Loop on columns first */
    for(i=piece.dimY-1;i>=0;i--){                           /* Start from bottom */
      rotBlockMap[k++] = piece.blockMap[i*piece.dimX+j];    /* Create the new rotate array */
    }
  }
  
  for(j=0;j<piece.dimY;j++){                                /* Check for emptyness of the grid blocks before copy */
    for(i=0;i<piece.dimX;i++){                              /* Rows and columns are swapped now */
      if(rotBlockMap[i*piece.dimY+j].isEmpty===1){          /* Working on rotate array */
        if(grid[getGridIndex(piece.gridPosX+j,piece.gridPosY+i)].isEmpty===1) {
          return;
        }
      }
    }
  }

  for(i=0;i<rotBlockMap.length;i++){
    piece.blockMap[i] = rotBlockMap[i];                      /* Copy the rotate array into piece's blockMap */
  }

  temp = piece.dimX;                                          /* Swap piece's numbers of rows and columns */
  piece.dimX = piece.dimY;
  piece.dimY = temp;
}

/** Move the piece vertically */
function verticalPieceMove() {

  generatePieceHitBox();                  /* Generate the piece hitbox for vertical movement */
  if(verticalHitBoxCheck()===0){          /* Check  if the piece has hitted on the vertical direction */
    piece.gridPosY++;                     /* If not, move the piece down of one block */
    return 0;                             /* Return no hit */
  }
  return 1;                               /* Return hit */
}  

/** Move the piece horizontally */
function horizontalPieceMove() {

  generatePieceHitBox();                  /* Generate the piece hitbox for horizontal movement */
  if(horizontalHitBoxCheck()===0){        /* Check  if the piece has hitted on the horizontal direction */
    piece.gridPosX += piece.direction;    /* If not, move the piece of one block */
    return 0;                             /* Return no hit */
  }
  return 1;                               /* Return hit */
}  

/* Main functions */
/** Check if a key was pressed ASYNC */
function keyPressed() {                         /* See doc https://p5js.org/reference/#/p5/keyPressed */
  if (keyCode === LEFT_ARROW) {                 /* If left arrow was pressed */
    piece.direction = -1;                       /* Set direction to left */
  } 
  else if (keyCode === RIGHT_ARROW) {           /* If right arrow was pressed */
    piece.direction = 1;                        /* Set direction to right */
  }
  else if (keyCode === DOWN_ARROW) {            /* If down arrow was pressed */
    piece.direction = 2;                        /* Set direction to down */
  }
  else if (key === " ") {                       /* If space was pressed */
    piece.tralsation = 1;                       /* Set traslation to on */
  }
} 

/** Check if a key was released ASYNC */
function keyReleased() {                                    /* See doc https://p5js.org/reference/#/p5/keyReleased */
  if (keyCode === LEFT_ARROW && piece.direction === -1) {   /* If left arrow was released and direction is left */                           
    piece.direction = 0;                                    /* Clear direction */
  } 
  if (keyCode === RIGHT_ARROW && piece.direction === 1) {           /* If right arrow was pressed */
    piece.direction = 0;                        
  }
  if (keyCode === DOWN_ARROW && piece.direction === 2) {            /* If down arrow was pressed */
    piece.direction = 0;                        
  }

  // piece.direction = 0;                          /* Clear direction */




  // piece.tralsation = 0;                         /* Clear traslation *///think is not needed 

  return false;                                 /* Prevent any default behavior see doc */
}

function setup() {
  frameRate(30);
  noStroke();

  createCanvas(canvasSizeX, canvasSizeY);
  createGrid();
  spawnNewPiece();
}

function draw() {
  background(220);

  printGrid();
  printPiece();

  // print(key); // Display last key pressed.
  if((frameCount%piece.speed)===0){
    if(piece.tralsation === 1){               /* If a traslation is required */
      rotatePiece();                          /* Rotate the piece */
      piece.tralsation = 0;                   /* Clear traslation */
    }

    if(piece.direction===-1||piece.direction===1){
      horizontalPieceMove(); 
    }
    if(verticalPieceMove()==1){
      addToGrid();
      spawnNewPiece();
    }
    // piece.direction = 0;                          /* Clear direction */
    // generatePieceHitBox();
  }

}